{
    'name': "Gn Donations",
    'version': '14.0.0.1.1',
    'author': 'Garage Numérique',
    'category': 'Accounting',
    'description': """
        This module modify the fiscal receipt report so it is similar to Cerfa 11580.
        It allows to edit fiscal receipts for in-kind donation (i.e. 0€ donation).
    """,
    'depends': ['donation'],
    'data': [
        'views/donation_thanks_report.xml',
        'views/internal_layout.xml'
    ],
    'translate': True,
    'installable': True,
}
