# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models


class ConventionCollective(models.Model):
    _name = "gn_payroll.cc"
    _description = "Convention Collective"
    _order = 'idcc, name'

    def copy(self, default=None):
        raise UserError(_('Duplicating a company is not allowed. Please create a new company instead.'))

    name = fields.Char(string='Nom complet de la Convention Collective', required=True, store=True, readonly=False)
    active = fields.Boolean(default=True)
    idcc = fields.Integer(string="IDCC", help='Used to order Conventions Collectives in the switcher', default=10)