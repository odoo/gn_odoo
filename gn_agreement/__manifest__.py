{
    "name": "France - Contrats",
    "version": "16.0.0.0.1",
    "category": "Payroll",
    "summary": "Templates for French contracts and agreements",
    "author": "Le Garage Numérique",
    "maintainers": ["makayabou"],
    "website": "https://odoo.legaragenumerique.fr",
    "depends": [
        "agreement",
        "agreement_legal",
        "partner_company_type"
    ],
    "data": [
        "views/gn_agreement_bipartisan_agreement.xml",
        "views/gn_agreement_external_layout_striped_footer.xml",
        "data/gn_agreement_document_layout.xml",
        "data/gn_agreement_types.xml",
        "data/gn_agreement_convention_cadre_template.xml",
        "data/gn_agreement_convention_avenant_template.xml",
    ],
    "license": "LGPL-3",
}
