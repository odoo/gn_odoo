{
    'name': "Gn Discount",
    'version': '14.0.0.1.3',
    'author': 'Garage Numérique',
    'category': 'Sales',
    'description': """
        This module adds the ability to display the total before discount and the discount amount on sales orders and invoices.

        This version only works when no taxes are applied
    """,
    'depends': ['sale', 'sale_discount_total'],
    'data': [
        'views/sale_view.xml',
        'views/sale_report.xml',
        'views/sale_portal.xml',
        'views/invoice_view.xml',
        'views/invoice_report.xml'
    ],
    'translate': True,
    'translations': [
        ('fr_FR', 'i18n/gn_discount.fr_FR.po'),
    ],
    'installable': True,
}
