from odoo import api, fields, models

class AccountMove(models.Model):
    _inherit = 'account.move'
    amount_undiscounted = fields.Monetary(compute='_compute_amount_undiscounted', store=True)

    @api.depends('amount_untaxed', 'amount_discount')
    def _compute_amount_undiscounted(self):
        for record in self:
            record.amount_undiscounted = record.amount_untaxed + record.amount_discount

class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'
    price_subtotal_before_discount = fields.Monetary(compute='_compute_amount_undiscounted', store=True, string="Subtotal", translate="True")

    @api.depends('price_unit', 'quantity')
    def _compute_amount_undiscounted(self):
        for record in self:
            record.price_subtotal_before_discount = record.price_unit * record.quantity
