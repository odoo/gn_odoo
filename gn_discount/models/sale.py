from odoo import api, fields, models

class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    price_subtotal_before_discount = fields.Monetary(readonly=True, string="Subtotal", translate=True, compute='_compute_price_subtotal_before_discount')

    @api.depends('price_unit', 'discount', 'product_uom_qty')
    def _compute_price_subtotal_before_discount(self):
        for line in self:
            line.price_subtotal_before_discount = line.price_unit * line.product_uom_qty