from odoo import models, fields

class GnPayrollHrEmployee(models.Model):
    _inherit = 'hr.employee'
    employee_type = fields.Selection([
        ('employee', 'Employé'),
        ('student', 'Étudiant'),
        ('trainee', 'Stagiaire'),
        ('volunteer', 'Volontaire'),
        ('benevolent', 'Bénévole'),
        ], string='Employee Type', default='employee', required=True,
        help="The employee type. Although the primary purpose may seem to categorize employees, this field has also an impact in the Contract History. Only Employee type is supposed to be under contract and will have a Contract History.")