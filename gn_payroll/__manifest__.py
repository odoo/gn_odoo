{
    "name": "France - Paye",
    "version": "16.0.0.0.2",
    "category": "Payroll",
    "summary": "Configuration de la paie",
    "author": "Le Garage Numérique",
    "maintainers": ["makayabou"],
    "website": "https://odoo.legaragenumerique.fr",
    "depends": [
        "payroll",
        "hr_contract"
    ],
    "data": [
        "data/gn_payroll_cc.xml",
        "data/gn_payroll_contract.xml",
        "data/gn_payroll_salary.xml",
        "data/gn_payroll_sickness.xml",
        "data/gn_payroll_time.xml",
        "data/gn_payroll_universal.xml",
        "data/gn_payroll_structure.xml",
        "views/gn_payroll_company.xml",
        "security/ir.model.access.csv"
    ],
    "license": "LGPL-3",
}
