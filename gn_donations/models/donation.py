# -*- coding: utf-8 -*-
from odoo import fields, models
import num2words

class TaxReceipt(models.Model):
    _inherit = 'donation.tax.receipt'
    amount_in_words = fields.Char(compute='_compute_amount_in_words', string='Amount in Words')

    def _compute_amount_in_words(self):
        for record in self:
            record.amount_in_words = num2words.num2words(record.amount, lang='fr')


from odoo import _, api
from odoo.exceptions import UserError
from odoo.tools.misc import format_amount

from odoo.addons.account import _auto_install_l10n
# Rewriting donation to allow 0€ fiscal receipts
class DonationDonation(models.Model):
    _inherit = 'donation.donation'

    def validate(self):
        check_total = self.env["res.users"].has_group(
            "donation.group_donation_check_total"
        )
        for donation in self:
            if donation.donation_date > fields.Date.context_today(self):
                raise UserError(
                    _(
                        "The date of donation %s should be today "
                        "or in the past, not in the future!"
                    )
                    % donation.number
                )
            if not donation.line_ids:
                raise UserError(
                    _(
                        "Cannot validate donation %s because it doesn't "
                        "have any lines!"
                    )
                    % donation.number
                )

            '''
            # The part we don't want, in order ta validate 0€ fiscal receipts
            if donation.currency_id.is_zero(donation.amount_total):
                raise UserError(
                    _("Cannot validate donation %s because the " "total amount is 0!")
                    % donation.number
                )
            '''

            if donation.state != "draft":
                raise UserError(
                    _(
                        "Cannot validate donation %s because it is not "
                        "in draft state."
                    )
                    % donation.number
                )

            if check_total and donation.currency_id.compare_amounts(
                donation.check_total, donation.amount_total
            ):
                raise UserError(
                    _(
                        "The amount of donation %s (%s) is different "
                        "from the sum of the donation lines (%s)."
                    )
                    % (
                        donation.number,
                        format_amount(
                            self.env, donation.check_total, donation.currency_id
                        ),
                        format_amount(
                            self.env, donation.amount_total, donation.currency_id
                        ),
                    )
                )
            full_in_kind = all([line.in_kind for line in donation.line_ids])
            if not donation.payment_mode_id and not full_in_kind:
                raise UserError(
                    _(
                        "Payment Mode is not set on donation %s (only fully "
                        "in-kind donations don't require a payment mode)."
                    )
                    % donation.number
                )

            vals = {"state": "done"}
            if full_in_kind and donation.payment_mode_id:
                vals["payment_mode_id"] = False

            if not full_in_kind:
                move_vals = donation._prepare_donation_move()
                # when we have a full in-kind donation: no account move
                if move_vals:
                    move = self.env["account.move"].create(move_vals)
                    move.action_post()
                    vals["move_id"] = move.id
            else:
                donation.message_post(
                    body=_("Full in-kind donation: no account move generated")
                )

            receipt = donation.generate_each_tax_receipt()
            if receipt:
                vals["tax_receipt_id"] = receipt.id

            donation.write(vals)
            if donation.bank_statement_line_id:
                donation._reconcile_donation_from_bank_statement()
            donation.partner_id._update_donor_rank()
        return

    def generate_each_tax_receipt(self):
        self.ensure_one()
        receipt = False
        if (
            self.tax_receipt_option == "each"
            and not self.tax_receipt_id
            #and not self.company_currency_id.is_zero(self.tax_receipt_total)
        ):
            receipt_vals = self._prepare_each_tax_receipt()
            receipt = self.env["donation.tax.receipt"].create(receipt_vals)
        return receipt