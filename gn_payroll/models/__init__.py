# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from . import gn_payroll_cc
from . import gn_payroll_company
from . import gn_payroll_employee